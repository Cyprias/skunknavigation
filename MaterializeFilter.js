/********************************************************************************************\
	File Name:      MaterializeFilter.js
	Purpose:        Catch the F7B1 00A1: Materialize event.
	Creator:        Cyprias
	Date:           10/28/2015
	License:        MIT License	(http://opensource.org/licenses/MIT)
\*********************************************************************************************/

var MAJOR = "MaterializeFilter-1.0";
var MINOR = 190328;

(function (factory) {
	// Check if script was loaded via require().
	if (typeof module === 'object' && module.exports) {
		module.exports = factory();
	} else {
		// Script was loaded via swx file, set as a global object.
		// eslint-disable-next-line no-undef
		MaterializeFilter10 = factory();
	}
}(function () {
	var core;
	if (typeof LibStub !== "undefined") {
		core = LibStub.newLibrary(MAJOR, MINOR);
		if (!core) return LibStub(MAJOR);
	} else {
		core = {};
	}
	core.MAJOR = MAJOR;
	core.MINOR = MINOR;
	
	var debugging = false;

	var mtyMaterialize       = 0xf7b100A1 - 0x100000000;

	core.debug = function debug(szMsg) {
		if (debugging == true) {
			skapi.OutputSz(core.MAJOR + ": " + szMsg + "\n", opmConsole);
		}
	};

	var handler = {};
	handler.OnRawServerMessage = function fOnRawServerMessage(mty) {
		switch(mty) {
		case mtyMaterialize:
			fireCallback();
		}
	};

	var _callbacks = [];
	core.addCallback = function addCallback(method, thisArg) {
		_callbacks.push({method: method, thisArg: thisArg});
		if (_callbacks.length == 1) {
			skapi.AddHandler(mtyMaterialize,  handler);
		}
	};
	
	core.removeCallback = function removeCallback(method, thisArg) {
		// eslint-disable-next-line no-restricted-syntax
		for (var i = _callbacks.length - 1; i >= 0; i--) {
			if (_callbacks[i].method == method) {
				if (thisArg && thisArg != _callbacks[i].thisArg) continue;
				_callbacks.splice(i, 1);
			}
		}
		if (_callbacks.length == 0) {
			skapi.RemoveHandler(mtyMaterialize,  handler);
		}
	};

	function fireCallback() {
		// eslint-disable-next-line no-restricted-syntax
		for (var i = _callbacks.length - 1; i >= 0; i--) {
			_callbacks[i].method.apply(_callbacks[i].thisArg, arguments);
		}
	}

	return core;
}));