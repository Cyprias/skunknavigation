/********************************************************************************************\
	File Name:      SkunkNavigation.js
	Purpose:        Navigation engine for Skunkworks. 
	Creator:        Cyprias
	Date:           06/17/2015
	License:        MIT License	(http://opensource.org/licenses/MIT)
	Requires:       SkunkTimer-1.0.js, AnimationFilter.js, 
	Optional:       MaterializeFilter.js, ToggleObjectVisibilityFilter.js
\*********************************************************************************************/

var MAJOR = "SkunkNavigation-1.0";
var MINOR = 200209;

(function (factory) {
	if (typeof module === 'object' && module.exports) {
		module.exports = factory();
	} else {
		// eslint-disable-next-line no-undef
		SkunkNavigation10 = factory();
	}
}(function () {
	
	var core;
	if (typeof LibStub !== "undefined") {
		core = LibStub.newLibrary(MAJOR, MINOR);
		if (!core) return LibStub(MAJOR);
	} else {
		core = {};
	}
	
	core.MAJOR = MAJOR;
	core.MINOR = MINOR;
	core.Error = Error;
	
	core.setInterval = setInterval;
	core.setTimeout = setTimeout;
	core.setImmediate = setImmediate;
	
	var version                     = MAJOR + "." + MINOR;
	var debugging                   = true;
	
	// settings
	var defaultFaceVariance         = 5;                    // When turning to face a direction, turn within x degrees. We need some variance since it goes off a timer that continuously checks our heading.
	var adjustHeadingVariance       = 10;                   // If our heading is x degres off what it should be, face the proper heading.
	var turnDistanceBuffer          = 1.5;                  // When predicting how far our turn will move us away from path, add another 40% to the distance needed to turn.
	var distWalk                    = 10 / 240;             // Walk when we get this close 
	var distArriveMin               = 10 / 240;		        // Stop when we get this close 
	var defaultTimeout              = 1000 * 60 * 5;
	var doorAheadDist               = 1 / 240;              // Search for doors x meters ahead of us to open.
	var stuckSpeed                  = 0.0000049 * 0.25;     // Walk speed, distance/elapsed. 0.00001 in peace mode. 0.0000049 in caster. We check where we've been and how long ago to see how far we've traveled compared to walk speed to determin stuckness.
	
	// If we get stuck while on course, turn this many degrees away from our current heading. 180 is complete opposite direction, we want to go between 90 and 180.
	// 180*(3/4)=135. 180*(4/5)=144. 180*(5/6)=150. 150*(12/11)=165
	var unstuckHeadingOffset        = 180 * (5 / 6);        
	

	//var exitPortalTimeout           = 1000 * 25;            // Includes time to enter portal. Should be rewritten.
		
	// Legacy SkunkNav route stuff.
	var routeTimeout                = 60 * 1000;            // Default route timeout in ms.

	//var degreeSymbol = String.fromCharCode(9);
	var degreeSymbol = String.fromCharCode(176); // °

	// Constants, don't touch.
	var cmeterPerMU                 = 240; 
	
	//var doorCloseTime               = 5 * 60 * 1000; // Time a door stays open.
	core.openedDoors = {};
	var footprints                 = [];

	var AnimationFilter     = (typeof LibStub !== "undefined" && LibStub.getLibrary("AnimationFilter-1.1", true)) || (typeof require === "function" && require("SkunkNavigation\\AnimationFilter"));
	var MaterializeFilter     = (typeof LibStub !== "undefined" && LibStub.getLibrary("MaterializeFilter-1.0", true)) || (typeof require === "function" && require("SkunkNavigation\\MaterializeFilter"));

	core.getVersion = function getVersion() {
		return version;
	};

	core.console = function _console() {
		var args = Array.prototype.slice.call(arguments);
		skapi.OutputLine("[SkunkNavigation] " + args.join(', '), opmConsole);
	};

	core.debug = function debug() {
		if (debugging == true) {
			var args = Array.prototype.slice.call(arguments);
			core.console("[D] " + args.join(', '));
		}
	};

	core.info = function info(szMsg) {
		skapi.OutputSz("[SkunkNavigation] " + szMsg + "\n", opmChatWnd, cmcPaleBlue);
		core.console(szMsg);
	};
	
	core.simpleUUID = function simpleUUID() {
		return Math.floor((1 + Math.random()) * 0x10000).toString(16)
			.substring(1);
	};

	/*
		headingTurn: Return the angle from our current heading needed to face the intended heading. Positive number = turn right, negitive number = turn left.
	*/
	core.headingTurn = function headingTurn(params) { //intendedHeading, currentHeading
		//if (typeof currentHeading === "undefined") currentHeading = skapi.maplocCur.head;
		var heading = params.heading;
		var current = params.current || skapi.maplocCur.head;
		var heading = ((heading - current) % 360);
		while (heading < 0) {
			heading += 360; // % in jscript doesn't touch negative numbers...
		}
		if (heading > 180) {
			heading -= 360;
		}
		return heading;
	};

	core.getLineLength = function getLineLength(x, y, x0, y0) {
		return Math.sqrt((x -= x0) * x + (y -= y0) * y);
	};
	
	/*
		getClosestSpot: Return the closest X Y coordinates from x to a line between x1 and x2.
	*/
	core.getClosestSpot = function getClosestSpot(x, y, x1, y1, x2, y2) {
		var A = x - x1;
		var B = y - y1;
		var C = x2 - x1;
		var D = y2 - y1;

		var dot = A * C + B * D;
		var len_sq = C * C + D * D;
		var param = dot / len_sq;

		var xx, yy;

		if (param < 0 || (x1 == x2 && y1 == y2)) {
			xx = x1;
			yy = y1;
		} else if (param > 1) {
			xx = x2;
			yy = y2;
		} else {
			xx = x1 + param * C;
			yy = y1 + param * D;
		}

		var dx = x - xx;
		var dy = y - yy;

		var tx = x - dx;
		var ty = y - dy;

		return {x: tx, y: ty};
	};
	
	/*
		getClosestSpotOnMaplocLine: Return the closest spot between two maplocs.
	*/
	core.getClosestSpotOnMaplocLine = function getClosestSpotOnMaplocLine(maplocA, maplocB, maplocC) {
		var	aX = maplocA.lng;
		var	aY = maplocA.lat;
		var bX = maplocB.lng;
		var bY = maplocB.lat;
		var cX = maplocC.lng;
		var cY = maplocC.lat;
		return core.getClosestSpot(aX, aY, bX, bY, cX, cY);
	};
	
	
	core.getClosestMaplocOnMaplocLine = function getClosestMaplocOnMaplocLine(lineMaplocA, lineMaplocB, maplocC) {
		var spot = core.getClosestSpotOnMaplocLine(lineMaplocA, lineMaplocB, maplocC);
		return skapi.MaplocFromLatLng(spot.y, spot.x);
	};
	
	/*
		meters: Convert map units to meters.
	*/
	core.meters = function meters(n) {
		return n * cmeterPerMU;
	};
	
	/*
		round: Round a number.
	*/
	core.round = function round(rnum, rlength) { // Arguments: number to round, number of decimal places
		if (rlength == null) rlength = 0;
		return Math.round(rnum * Math.pow(10, rlength)) / Math.pow(10, rlength);
	};
	
	/*
		getDesiredSpeed: Return walk or run based on our distance to a maploc.
	*/
	core.getDesiredSpeed = function getDesiredSpeed(toMaploc) {
		if (typeof toMaploc !== "undefined") {
			var dist = skapi.maplocCur.Dist2DToMaploc(toMaploc);
			if (dist <= distWalk) {
				return cmidWalkForward;
			}
		}
		return cmidAutoRun;
	};

	/*
		getDirectionCoords: Return a set of coordinates in a specific direction and distance of another set of coordinates.
	*/
	core.getDirectionCoords = function getDirectionCoords(mX, mY, direction, distance) {
		direction += 90;	// Rad sees 0 as east, adding 90 seems to make it north.
		var r = core.radians(direction);
		var tX = mX - (distance * Math.cos(r));
		var tY = mY + (distance * Math.sin(r));
		return {x: tX, y: tY};
	};
	
	/*
		getTurnPositions: Return a list of coordinates in front of us in one degree intervals until a certain turn heading. Used to determin if our turn will move us too far away from a line. 
	*/
	core.getTurnPositions = function getTurnPositions(turnRight, degrees, distance) {
		// turnRight: Whether we're turning left or right.
		// degrees: Degrees we'll be turning, See core.headingTurn();
		// distance: Our turning speed, related to our character's run speed.
		if (typeof turnRight === "undefined")
			turnRight = true;
		if (typeof degrees === "undefined")
			degrees = 180;
		if (typeof distance === "undefined")
			distance = (0.876 * skapi.vRun) / 2;
		
		core.debug("<getTurnPositions> turnRight: " + turnRight + ", degrees: " + degrees + ", distance :" + distance);
		
		var positions = [];

		var centerHeading;
		if (turnRight == true) {
			centerHeading = skapi.maplocCur.head + 90;
		} else {
			centerHeading = skapi.maplocCur.head - 90;
		}
		
		// center coords, about 90deg tot he left or right, distance should be distance. 
		var cCoords = core.getDirectionCoords(skapi.maplocCur.lng, skapi.maplocCur.lat, centerHeading, distance);

		var startingDegress = skapi.maplocCur.head;
		
		if (turnRight != true) {
			startingDegress -= 180;
		}

		//core.debug("startingDegress: " + startingDegress);
		
		var angle;
		var pX, pY;
		if (turnRight == true) {
			// eslint-disable-next-line no-restricted-syntax
			for (var a = startingDegress; a < startingDegress + degrees; a++) {
				angle = core.radians(a);
				pX = cCoords.x + (0 - distance * Math.cos(angle));
				pY = cCoords.y + (0 + distance * Math.sin(angle));

				positions.push({x: pX, y: pY});
			}
		} else {
			// eslint-disable-next-line no-restricted-syntax
			for (var a = startingDegress; a > startingDegress - degrees; a--) {
				angle = core.radians(a);
				pX = cCoords.x + (0 - distance * Math.cos(angle));
				pY = cCoords.y + (0 + distance * Math.sin(angle));

				positions.push({x: pX, y: pY});
			}
		}
		
		return positions;
	};
	
	/*
		getMaxPositionDistanceFromLine: Return a distance a turn will move us away from a line.
	*/
	core.getMaxPositionDistanceFromLine = function getMaxPositionDistanceFromLine(lineMaplocA, lineMaplocB, positions) {
		var max = 0;
		
		var pos;
		var closestSpot;
		var dist;
		// eslint-disable-next-line no-restricted-syntax
		for (var i = 0; i < positions.length; i++) {
			pos = positions[i];
			
			//maploc = skapi.MaplocFromLatLng(pos.y, pos.x);
			closestSpot = core.getClosestSpot(pos.x, pos.y, lineMaplocA.lng, lineMaplocA.lat, lineMaplocB.lng, lineMaplocB.lat);
			dist = core.getLineLength(closestSpot.x, closestSpot.y, pos.x, pos.y);
			
			//core.debug("pos.x: " + pos[0] + ", pos.y: " + pos[1] + ", closestSpot.x: " + closestSpot.x + ", closestSpot.y: " + closestSpot.y + ", dist: " + dist);
			if (dist > max) {
				max = dist;
			}
		}
		
		//skapi.MaplocFromLatLng(spot.y, spot.x);
		//getClosestSpotOnMaplocLine
		
		return max;
	};

	/*
		getAngleToAvoidCircle: Return the angle from the center of a circle to the edge. 
			radius: Radius of the circle.
			distance: Distance from the circle center.
	*/
	core.getAngleToAvoidCircle = function getAngleToAvoidCircle(radius, distance) {
		return Math.asin(radius / distance);
	};
	
	/*
		getHeadingXY: Return heading from one set of coords to another.
	*/
	core.getHeadingXY = function getHeadingXY(fX, fY, tX, tY) {
		var dx = tX - fX;
		var dy = tY - fY;
		var headRad = Math.atan2(dx, dy);
		if (headRad < 0) headRad += 2 * Math.PI;
		return core.degrees(headRad);
	};
	
	/*
		getRelativeAngleToAvoidCircle: Return the angle to the edge of a circle relative to the heading to the circle. 
	*/
	core.getRelativeAngleToAvoidCircle = function getRelativeAngleToAvoidCircle(headingToCircle, radius, distance) {
		core.debug("<getRelativeAngleToAvoidCircle>");
		var angle;
		if (distance >= radius) {
			var angle = core.getAngleToAvoidCircle(radius, distance);
			var headingTurn = core.headingTurn({heading: headingToCircle});
			if (headingTurn > 0) {// Circle is to our right, we want to turn left.
				angle = (headingToCircle - core.degrees(angle)) % 360;
			} else { // Circle is to our left, we want to turn right.
				angle = (headingToCircle + core.degrees(angle)) % 360;
			}
		}
		if (typeof angle !== "undefined" && angle < 0) {
			angle += 360;
		}
		return angle;
	};

	
	var Course = core.Course = function()  {
		//this.maploc             = undefined;
		//this.aco                = undefined;
		//this.direction          = undefined;
		this.timeout            = defaultTimeout;
		this.creationTime       = (new Date()).getTime();
		this.distArrive         = distArriveMin;
		
		//this.lineDist           = undefined;// = distArrive * 2;
		this.startingTime       = (new Date()).getTime();
		this.resumeTime         = (new Date()).getTime();
		this.pauseTime          = 0;
		this.totalPauseTime     = 0;
		this.tickCount          = 0;
		this.zigzag             = false;
		this.forceWalk          = false; // Force walking.
		this.avoidPortals       = false;
		this.cStuckSpeed        = stuckSpeed;
		this.erratic            = false; // random direction when stuck.
		//this.startingMaploc     = undefined;
	
		this.stuckStarted       = 0;
		
		//this.stuckStartedPoint  = undefined;
		this.stuckRight         = false;
		this.stuckRatio         = 0;
		this.stuckDuration      = 0;
		this.lastStuckTime      = 0;
		
		this.returningToLine    = false;
		this.lastErraticChange  = 0;
		
		this.cmidZigZag         = cmidMovementStrafeLeft; // we alternate the direction.
	};
	
	Course.prototype.setErratic = function setErratic(value) {
		this.erratic = value;
	};
	
	Course.prototype.getErratic = function getErratic() {
		return this.erratic;
	};

	Course.prototype.setStuckSpeed = function setStuckSpeed(value) {
		this.cStuckSpeed = value;
	};
	
	Course.prototype.setAvoidPortals = function setAvoidPortals(value) {
		this.avoidPortals = value;
	};
	
	Course.prototype.getAvoidPortals = function getAvoidPortals() {
		return this.avoidPortals;
	};
	
	Course.prototype.setWalk = function setWalk(value) {
		this.forceWalk = value;
	};
		
	Course.prototype.getWalk = function getWalk() {
		return this.forceWalk;
	};
	
	Course.prototype.setZigZag = function setZigZag(value) {
		this.zigzag = value;
	};
	
	Course.prototype.setLineDist = function setLineDist(value) {
		this.lineDist = value;
	};
	
	Course.prototype.setMaploc = function setMaploc(value) {
		this.maploc = value;
	};
	
	Course.prototype.getMaploc = function getMaploc() {
		if (typeof this.maploc !== "undefined") {
			return this.maploc;
		} else if (typeof this.aco !== "undefined") {
			return this.aco.maploc;
		}
	};
	
	Course.prototype.setAco = function setAco(value) { // Run to a aco's maploc rather than a specific maploc, so if they move we move with them.
		this.aco = value;
	};

	Course.prototype.getAco = function getAco() {
		return this.aco;
	};
	
	Course.prototype.setDirection = function setDirection(value) {
		this.direction = value;
	};
	
	Course.prototype.getDirection = function getDirection() {
		return this.direction;
	};
	
	Course.prototype.setTimeout = function _setTimeout(value) {
		this.timeout = value;
	};
	
	Course.prototype.getTimeout = function getTimeout() {
		return this.timeout;
	};
	
	Course.prototype.getAge = function getAge() {
		return (new Date()).getTime() - this.creationTime;
	};
	
	Course.prototype.setDistArrive = function setDistArrive(value) {
		this.distArrive = value;
		if (typeof this.lineDist === "undefined") this.lineDist = value * 3; 
	};

	Course.prototype.distanceToMaploc = function distanceToMaploc() {
		var m = this.getMaploc();
		if (typeof m !== "undefined") {
			return skapi.maplocCur.Dist2DToMaploc(m);
		}
	};
	
	Course.prototype.hasArrived = function hasArrived() {
		var m = this.getMaploc();
		if (typeof m !== "undefined") {
			if (m.z == 0) { // If Z coord is 0 use 2D distance.
				return skapi.maplocCur.Dist2DToMaploc(m) <= this.distArrive;
			} else {
				return skapi.maplocCur.Dist3DToMaploc(m) <= this.distArrive;
			}
		}
	};
	
	Course.prototype.hasTimedout = function hasTimedout() {
		if (typeof this.startingTime !== "undefined") {
			var totalElapsed = (new Date()).getTime() - this.startingTime;
			totalElapsed -= this.totalPauseTime;
			return totalElapsed > this.timeout;
		}
	};
	
	Course.prototype.getTotalElapsed = function getTotalElapsed() {
		if (typeof this.startingTime !== "undefined") {
			var totalElapsed = (new Date()).getTime() - this.startingTime;
			totalElapsed -= this.totalPauseTime;
			return totalElapsed;
		}
	};

	Course.prototype.start = function start() {
		this.startingTime = (new Date()).getTime();
		if (typeof this.startingMaploc === "undefined") {
			this.startingMaploc = skapi.maplocCur;
		}
		core.clearFootprints();
		this.resumeTime = (new Date()).getTime();
	};
	
	Course.prototype.setStartingMaploc = function setStartingMaploc(value) {
		this.startingMaploc = value;
	};

	Course.prototype.getTickCount = function getTickCount() {
		return this.tickCount;
	};
	
	Course.prototype.isPaused = function isPaused() {
		return this.pauseTime > 0;
	};
	
	Course.prototype.pause = function pause() {
		this.pauseTime = (new Date()).getTime();
	};

	Course.prototype.resume = function resume() {
		var pauseElapsed = (new Date()).getTime() - this.pauseTime;
		this.totalPauseTime += pauseElapsed;
		this.pauseTime = 0;
		core.clearFootprints();
		this.resumeTime = (new Date()).getTime();
	};

	Course.prototype.getStuckElapsed = function getStuckElapsed() {
		return (new Date()).getTime() - this.stuckStarted;
	};
	
	Course.prototype.getETA = function getETA() {
		return core.getETA(this.getMaploc(), this.distArrive);
	};

	Course.prototype.getStuckRatio = function getStuckRatio() {
		return this.stuckRatio;
	};

	Course.prototype.getStuckDuration = function getStuckDuration() {
		return this.stuckDuration;
	};
	
	Course.prototype.getStuckStarted = function getStuckStarted() {
		return this.stuckStarted;
	};

	Course.prototype.tickSync = function tickSync() {
		core.debug("<tickSync>");
		this.tickCount += 1;
		if (this.isPaused()) {
			core.debug("Auto resuming course...");
			this.resume();
		}

		//var aCoords = GetDirectionCoords(skapi.maplocCur.lng, skapi.maplocCur.lat, skapi.maplocCur.head, 0); //0.01 / cmeterPerMU
		//var aMaploc = skapi.MaplocFromLatLng(skapi.maplocCur.lat, skapi.maplocCur.lng);
		
		// Open any doors a head of us.
		var coacoDoors;
		var aheadHeading = skapi.maplocCur.head; //lol

		var _doorAheadDist = doorAheadDist;
			
		// If we have a maploc, make sure we're facing it before checking for doors ahead of us. I'm doing this because sometimes in the mansion basement (not the dungeon) you can lag and bounce back from the door.
		var m = this.getMaploc();
		if (typeof m !== "undefined" && m != null) {
			aheadHeading = skapi.maplocCur.HeadToMaploc(m);
			_doorAheadDist = Math.min(_doorAheadDist, skapi.maplocCur.Dist2DToMaploc(m));
		} else if (typeof this.direction !== "undefined") {
			aheadHeading = this.direction;
		}
		
		//core.debug("aheadHeading: "  + aheadHeading);
		//core.debug("doorAheadDist: "  + doorAheadDist);
		
		var aheadCoords = core.getDirectionCoords(skapi.maplocCur.lng, skapi.maplocCur.lat, aheadHeading, _doorAheadDist);
		var aheadMaploc = skapi.MaplocFromLatLng(aheadCoords.y, aheadCoords.x);
		var coacoDoors = core.getDoorsNearLine(skapi.maplocCur, aheadMaploc);

		if (coacoDoors.Count > 0) {
			core.debug("coacoDoors: " + coacoDoors.Count);
			var acoDoor;
			// eslint-disable-next-line no-restricted-syntax
			for (var i = 0; i < coacoDoors.Count; i++) {
				acoDoor = coacoDoors.Item(i);
				if (core.openedDoors[acoDoor.oid]) {
					core.debug("door already open", acoDoor.oid);
					continue;
				}
				try {
					core.strikeReadyKeySync();
				} catch (err) {
					//return core.console("0xd82f Error striking ready key.", err);
					throw err;
				}

				core.debug("Opening door... maplocCur.z: " + skapi.maplocCur.z + ", maploc.z: " + acoDoor.maploc.z + " (" + (skapi.maplocCur.z - acoDoor.maploc.z) + ")");
				core.info("Opening door (" + (acoDoor && acoDoor.oid) + ")...");
				try {
					core.openDoorSync({aco: acoDoor});
				} catch (err) {
					//return core.console("0x9ab7 Error while opening door", err);
					throw err;
				}
				
				// Wait a moment before continuing.
				skapi.WaitEvent(1000, wemFullTimeout); 

				try {
					core.faceSync({heading: aheadHeading}); // Face our orginial heading again in case opening door turned us.
				} catch (err) {
					//return core.console("0xbff1 Error while facing", err);
					throw err;
				}

				core.clearFootprints();// Since we've been standing at the door for a second. 
				return;
			}
		}

		
		var stuckMaxTime = 30 * 1000;
		
		var bRatio = this.stuckRatio;
		
		this.stuckDuration = 0;
		
		//var runTime = (new Date()).getTime() - this.resumeTime;
		
		this.stuckRatio = 0;
		var elapsed;
		var dist;
		var speed;
		// eslint-disable-next-line no-restricted-syntax
		for (var i = footprints.length - 1; i > 0; i--) {
			elapsed = (new Date()).getTime() - footprints[i].time;
			if (elapsed < 3000) continue;
			if (elapsed > stuckMaxTime) continue;
			
			dist = skapi.maplocCur.Dist2DToMaploc(footprints[i].maploc);
			speed = dist / elapsed;

			if (speed <= this.cStuckSpeed) {
				this.stuckStarted = this.stuckStarted || (new Date()).getTime();
				this.stuckDuration = (new Date()).getTime() - this.stuckStarted;//elapsed;

				this.stuckRatio = this.stuckDuration / stuckMaxTime;
				this.stuckRatio = Math.min(this.stuckRatio, 1);
				
				core.info("We're stuck! elapsed: " + core.round(elapsed) + ", dist: " + core.round(dist * 240, 1) + "m stuckRatio: " + core.round(this.stuckRatio, 2));

				this.stuckStartedPoint = footprints[i].maploc;
				this.lastStuckTime = (new Date()).getTime();

				if (this.erratic == true && (new Date()).getTime() - this.lastErraticChange > 5 * 1000) {
					this.direction = ((this.direction + 90) + (Math.random() * 180) / 2) % 360;
					core.info("Changing direction to " + this.direction + " (erratic)");
					this.lastErraticChange = new Date();
				}
				break;
			}
		}
		
		if (this.stuckRatio == 0 && bRatio > 0) {
			core.info("We're not stuck anymore!");
			this.stuckStarted = 0;
		}
		
		
		if (this.stuckRatio > 0) {
			//var stuckRatio = (stuckTime / 15000);
			//var stuckRatio = (stuckTime / stuckMaxTime);
			//stuckRatio = Math.min(stuckRatio, 1);

			var headOffset = unstuckHeadingOffset * this.stuckRatio;
			
			core.debug("stuckDuration: " + this.stuckDuration + ", stuckRatio: " + core.round(this.stuckRatio, 2) + ", headOffset: " + core.round(headOffset));
			var cHead = skapi.maplocCur.head;
			var nHead = cHead;
			if (this.stuckRight == true) {
				nHead += headOffset;
			} else {
				nHead -= headOffset;
			}
			this.stuckRight = !this.stuckRight;

			
			try {
				core.faceSync({heading: nHead});
			} catch (err) {
				//return core.console("0x07bc Error while facing", err);
				throw err;
			}
			
			
			if (!core.amMoving()) {
				core.debug("Start moving...");
				core.startMovingSync(m, this.forceWalk);
			} else {
				core.debug("We're moving...");
			}

			// Give us time to move away.
			var keyTime = Math.min(this.stuckDuration / 10, 3000); // 10 cause it's easy to go too far while indoors. I'd rather be stuck for longer than get stuck in a different place later. 3000ms max.
			core.debug("Short wait to move away... (" + keyTime + "ms)");

			skapi.Sleep(keyTime);

			if (this.stuckRatio > 0.15) {
				core.jump();
			}
			
			dist = skapi.maplocCur.Dist2DToMaploc(this.stuckStartedPoint);
			core.purgeFootprintsByDist(dist);

			try {
				core.faceSync({heading: cHead, variance: 10});
			} catch (err) {
				//return core.console("0xcc35 Error while facing", err);
				throw err;
			}
			
			return;
		}
		
		
		var portalNear = false;
		var avoidingPortal = false;
		if (this.avoidPortals == true) {
			// Default to walk distances.
			var avoidPortalDistance     = 10 / 240;
			var avoidPortalRadius       = 5 / 240;
			if (animationState.running == true) {
				// We're running, widen our radius.
				avoidPortalDistance     = 20 / 240;
				avoidPortalRadius       = 10 / 240;
			}

			// Check for portals.
			var acf = skapi.AcfNew();
			acf.oty = otyPortal;
			acf.distMax = avoidPortalDistance;
			var coaco = acf.CoacoGetSorted(skapi.sortbyDist);
			if (coaco.Count > 0) {
				portalNear = true;
				var acoPortal = coaco.Item(0);

				var dist = skapi.maplocCur.Dist2DToMaploc(acoPortal.maploc);
				var headingToObject = skapi.maplocCur.HeadToMaploc(acoPortal.maploc);
				var headingTurn = core.headingTurn({heading: headingToObject, current: aheadHeading});

				//core.console("aheadHeading: " + core.round(aheadHeading,1) + ", headingToObject: " + core.round(headingToObject,1) + ", headingTurn: " + core.round(headingTurn, 1));
				
				if (Math.abs(headingTurn) < 90) {// Only avoid if the heading to object is within 90 degrees of our desired heading, anything above that and we're passing it.
					var angle = skapi.maplocCur.head;
					if (dist > avoidPortalRadius) {
						var angle2 = core.getRelativeAngleToAvoidCircle(headingToObject, avoidPortalRadius, dist);
						
						//core.debug("Angle around portal: " + core.round(angle));
						var headingTurn2 = core.headingTurn({heading: angle2});
						
						//core.console("headingTurn: " + core.round(headingTurn) + ", angle2: " + core.round(angle2) + ", headingTurn2: " + core.round(headingTurn2));
						
						if (headingTurn > 0) {// Circle is to our right, we want to turn left.
							if (headingTurn2 <= 0) { // Angle would turn us left.
								avoidingPortal = true;
								angle = angle2;
								core.info("Turning left around " + acoPortal.szName + "... (" + core.round(angle2) + degreeSymbol + ")");
							} else { // Heading would turn us right (at the portal). ignore.
								core.info(acoPortal.szName + " is right of us...");
							}
						} else {
							if (headingTurn2 >= 0) {
								avoidingPortal = true;
								angle = angle2;
								core.info("Turning right around " + acoPortal.szName + "... (" + core.round(angle2) + degreeSymbol + ")");
							} else {
								core.info(acoPortal.szName + " is left of us...");
							}
						}
					} else if (dist <= (avoidPortalRadius / 2)) {
						// run away from it...
						angle = (headingToObject + 180);
						
						//core.debug("Angle AWAY from portal: " + core.round(angle,1));
						core.info("Turning AWAY from " + acoPortal.szName + "... (" + core.round(angle) + degreeSymbol + ")");
						avoidingPortal = true;
					}

					try {
						core.faceSync({heading: angle});
					} catch (err) {
						//return core.console("0xfa7c Error while facing", err);
						throw err;
					}
				}
			}
		}
		
		
		if (portalNear == false) {
			if (typeof m !== "undefined" && typeof this.startingMaploc !== "undefined") {
				var distToMaploc = skapi.maplocCur.Dist2DToMaploc(m);

				if (distToMaploc <= this.distArrive) {
					return;
				}

				var headtoMaploc = skapi.maplocCur.HeadToMaploc(m);
				
				var closestPoint = core.getClosestMaplocOnMaplocLine(this.startingMaploc, m, skapi.maplocCur);
				var dist = skapi.maplocCur.Dist2DToMaploc(closestPoint);

				core.debug("dist to line: " + core.round(dist, 4) + " (" + core.meters(dist, 2) + "m)");

				if (((new Date()).getTime() - this.lastStuckTime > 15000) && this.stuckStarted == 0 && dist > (this.lineDist || this.distArrive) && portalNear == false) {
					core.debug("Returning to line... (" + core.round(core.meters(dist), 1) + "m)");
					var headToLine = skapi.maplocCur.HeadToMaploc(closestPoint);
					if (Math.abs(skapi.maplocCur.head - headToLine) > adjustHeadingVariance) {
						core.debug("Facing line... (" + headToLine + ")");

						try {
							core.strikeReadyKeySync();
						} catch (err) {
							//return core.console("0xe1ee Error striking ready key.", err);
							throw err;
						}
						
						try {
							core.faceSync({heading: headToLine});
						} catch (err) {
							//return core.console("0x5818 Error while facing", err);
							throw err;
						}

						try {
							core.startMovingSync(closestPoint, this.forceWalk);
						} catch (err) {
							//return core.console("0x9d51 Error while moving forward", err);
							throw err;
						}
						
						return;
					}
				} else {
					if (Math.abs(skapi.maplocCur.head - headtoMaploc) > adjustHeadingVariance) {
						core.debug("Adjusting heading to face maploc...");
						
						//var turnDistance = (0.876 * skapi.vRun) * turnDistanceBuffer;

						var rHeading = core.headingTurn({heading: headtoMaploc});
						core.debug("we're not facing maploc, rHeading: " + rHeading);

						var turnPositions;
						if (rHeading > 0) {
							turnPositions = core.getTurnPositions(true, Math.abs(rHeading) / 2);
						} else {
							turnPositions = core.getTurnPositions(false, Math.abs(rHeading) / 2);
						}
						
						//	core.debug("turnPositions: " + turnPositions.length);
						
						
						//var maxTurnDistance = core.getMaxPositionDistanceFromLine(startingMaploc, maploc, turnPositions);
						var maxTurnDistance = 0;
						
						// Don't count all the maplocs cause the inital one might be far off. Instead just do the last one which could be the half way point of the turn, ie the furthest.
						if (turnPositions.length > 0) {
							var pos = turnPositions[turnPositions.length - 1];
							var closestSpot = core.getClosestSpot(pos.x, pos.y, this.startingMaploc.lng, this.startingMaploc.lat, m.lng, m.lat);
							maxTurnDistance = core.getLineLength(closestSpot.x, closestSpot.y, pos.x, pos.y);
						}
						
						
						maxTurnDistance *= turnDistanceBuffer;
						
						core.debug("turnPositions: " + turnPositions.length + ", maxTurnDistance: " + core.round(maxTurnDistance, 4) + " (" + core.round(core.meters(maxTurnDistance), 1) + "m)");
						
						if (maxTurnDistance > (this.lineDist || this.distArrive)) { //distArrive
							core.debug("Our turn would move us too far away from the line. Stopping before we turn...");

							try {
								core.strikeReadyKeySync();
							} catch (err) {
								//return core.console("0x15fe Error while striking ready key.", err);
								throw err;
							}
							
							core.debug("done stopping...");
						}
						
						core.debug("Facing maploc... (" + headtoMaploc + ")");

						try {
							core.faceSync({heading: headtoMaploc});
						} catch (err) {
							//return core.console("0x3c41 Error while facing", err);
							throw err;
						}
						
					}
				}
				
				distToMaploc = skapi.maplocCur.Dist2DToMaploc(m);
				
				//core.debug("distToMaploc: " + core.round(meters(distToMaploc),1) + "m, distWalk: " + core.round(meters(distWalk),1) + "m (" + (distToMaploc > distWalk) + ")");
				//core.debug("distToMaploc: " + core.round(meters(distToMaploc),1) + "m, distWalk: " + distWalk + "m (" + (distToMaploc > distWalk) + ")");
				
				//core.debug("amRunning: " + core.amRunning() + ", amWalking: " + core.amWalking());
				
				if (distToMaploc > distWalk && this.forceWalk == false) {
					// should be running.

					if (animationState.running != true) {
						core.debug("Starting auto running...");
						try {
							core.startAutoRunningSync();
						} catch (err) {
							//return core.console("0x127f Error while starting auto run", err);
							throw err;
						}
						return;
					}
				} else {
					// should be walking.
					// Walking auto turns off auto running.
					if (animationState.running == true) {
						core.debug("stop running...");
						try {
							core.strikeReadyKeySync();
						} catch (err) {
							//return core.console("0xc0d0 Error while striking ready key.", err);
							throw err;
						}
					}

					if (animationState.walkingForward != true) {
						core.debug("We're not walking, starting walking...");
						try {
							core.startWalkingSync();
						} catch (err) {
							//return core.console("0x0725 Error while walking forward", err);
							throw err;
						}
						core.debug("We should be walking now: " + animationState.walkingForward);
						return;
					}
				}
			} else if (typeof this.direction !== "undefined") {
				//core.debug("Facing direction... (" + this.direction + ")");

				try {
					core.faceSync({heading: this.direction, variance: 10});
				} catch (err) {
					//return core.console("0x90fd Error while facing", err);
					throw err;
				}
				
			}
		}

		var desiredSpeed = core.getDesiredSpeed(m);
		core.debug("dSpeed: " + desiredSpeed + ", fWalk: " + this.forceWalk + ", running: " + animationState.running + ", walking: " + animationState.walkingForward);
		if (this.forceWalk == true) {
			if (animationState.walkingForward != true) {
				core.debug("We should start walking (force)...");
				try {
					core.startWalkingSync();
				} catch (err) {
					//return core.console("0x104e Error while walking forward", err);
					throw err;
				}
			}
		} else {
			if (desiredSpeed == cmidAutoRun && animationState.running != true) {
				core.debug("We should start auto running...");
				try {
					core.startAutoRunningSync();
				} catch (err) {
					//return core.console("0x57bc Error while starting auto run", err);
					throw err;
				}
			} else if (desiredSpeed == cmidWalkForward && animationState.walkingForward != true) {
				core.debug("We should start walking...");
				try {
					core.startWalkingSync();
				} catch (err) {
					//return core.console("0xe2cb Error while walking forward", err);
					throw err;
				}
			}
		}

		if (this.zigzag == true && avoidingPortal == false) {
			if (this.cmidZigZag == cmidMovementStrafeLeft) {
				this.cmidZigZag = cmidMovementStrafeRight;
			} else {
				this.cmidZigZag = cmidMovementStrafeLeft;
			}

			try {
				core.strikeKeySync(this.cmidZigZag, 250);
			} catch (err) {
				//return core.console("0x2a0b Error while striking zig zag.", err);
				throw err;
			}
		}
		
	};
	
	// Async tick function.
	Course.prototype.tick = function tick(params, callback) {
		core.debug("<tick>");
		
		function resolve() {
			core.debug("<tick|resolve>");
			callback && core.setImmediate(core.applyMethod, callback, params && params.thisArg, arguments); // Call callback in a timer so other event handlers can process the event.
		}
		
		this.tickCount += 1;
		if (this.isPaused()) {
			core.debug("Auto resuming course...");
			this.resume();
		}
		
		//var doorDist = doorAheadDist;
		var desiredDirection = skapi.maplocCur.head; //lol
		
		var maploc = this.getMaploc();
		if (maploc) {
			desiredDirection = skapi.maplocCur.HeadToMaploc(maploc);
			
		//	doorDist = Math.min(doorDist, skapi.maplocCur.Dist2DToMaploc(maploc));
		} else if (typeof this.direction !== "undefined") {
			desiredDirection = this.direction;
		}
		
		// TODO: Check for doors
		
		// TODO: Avoid Portals
		
		var diff = Math.abs(skapi.maplocCur.head - desiredDirection);

		if (diff > 15) {
			core.info("Striking ready key...");
			return core.strikeReadyKey(undefined, function onStrikeReadyKey(err, results) {
				core.debug("<tick|onStrikeReadyKey>", err, results);
				if (err) return resolve(err);
				core.info("Facing " + desiredDirection + "...");
				core.face({
					heading: desiredDirection
				}, resolve);
			});
		}
		
		if (!core.amMoving()) {
			return core.startMoving({
				maploc: maploc
			}, resolve);
		}

		resolve();
	};

	/*
		goToAcoSync: Travel to a aco's position, useful when you expect your target to move while you navigate. 
	\*****************************************************************************
	core.goToAcoSync = function goToAcoSync( params ) {
		var aco             = params.aco
		var distArrive      = params.distArrive;
		var walk            = params.walk;
		var stopOnArrival   = params.stopOnArrival;
		var timeout         = params.timeout;
		var startingMaploc  = params.startingMaploc;
		var bailMethod      = params.bailMethod;
		var avoidPortals    = true;
		if (typeof params.avoidPortals !== "undefined") avoidPortals = params.avoidPortals;
		
		core.debug("<goToAcoSync> " + aco + ", distArrive: " + distArrive + ", walk: " + walk + ", stopOnArrival: " + stopOnArrival + ", timeout: " + timeout + ", startingMaploc: " + startingMaploc);
		var result = 0;
		var course = new Course();
		course.setAco(aco);
		if (typeof distArrive !== "undefined") {
			course.setDistArrive(distArrive);
		}
		if (typeof timeout !== "undefined") {
			course.setTimeout(timeout);
		}
		if (typeof stopOnArrival === "undefined") {
			stopOnArrival = true;
		}
		if (typeof startingMaploc === "undefined") {
			course.setStartingMaploc(startingMaploc);
		}
		
		if (typeof walk !== "undefined") {
			course.setWalk(walk);
		}
		
		course.setAvoidPortals(avoidPortals);
		//course.setZigZag(true);
		
		course.start();
		
		do {
			if (bailMethod) {
				if (bailMethod()) {
					break;
				}
			}

			course.tickSync();
			if (course.hasArrived()) {
				core.debug("We've arrived.");
				result |= flags.success;
				break;
			}
			skapi.WaitEvent(100, wemFullTimeout); 
		} while (!course.hasTimedout())

		if (course.hasTimedout()) {
			core.debug("Course has timed out...");
			result |= flags.timedOut;
		}

		if (stopOnArrival == true) {
			core.debug("Full Stop!");
			core.strikeReadyKeySync();
		}
			
		return result;
	};
	*/
	
	/*
		getDoorsNearMaploc: Return coaco of doors near a maploc.
	*/
	core.getDoorsNearMaploc = function getDoorsNearMaploc(maploc) {
		var acf = skapi.AcfNew();
		acf.oty = otyDoor;
		acf.distMax = 1 / cmeterPerMU;
		acf.maploc = maploc || skapi.maplocCur;
		core.debug("<getDoorsNearMaploc> " + acf.maploc.sz(4));
		
		return acf.CoacoGetSorted(skapi.sortbyDist);
	};
	
	/*
		getDoorsNearLine: Return coaco of doors near a line between two maplocs.
	*/

	//acf.distMax = 1 / cmeterPerMU;
	core.getDoorsNearLine = function getDoorsNearLine(maplocA, maplocB, maxDist) {
		if (typeof maxDist === "undefined") maxDist = 1 / cmeterPerMU;
		
		var acf = skapi.AcfNew();
		acf.oty = otyDoor;
		acf.maploc = skapi.maplocCur;

		var minZ = 0;
		var maxZ = 0;
		
		if (maplocA.z != 0 && maplocB.z != 0) {
			minZ = Math.min(maplocA.z, maplocB.z);
			maxZ = Math.max(maplocA.z, maplocB.z);
		} else if (maplocA.z != 0) {
			minZ = maplocA.z;// - 0.1;
			maxZ = maplocA.z;// + 0.1;
		} else if (maplocB.z != 0) {
			minZ = maplocB.z;// - 0.1;
			maxZ = maplocB.z;// + 0.1;
		}

		//	core.debug("minZ: " + minZ + ", maxZ: " + maxZ);
		
		//
		var coaco = acf.CoacoGetSorted(skapi.sortbyDist);
		
		var acoDoor;
		var closestSpot;
		var dist;
		// eslint-disable-next-line no-restricted-syntax
		for (var i = coaco.Count - 1; i >= 0; i--) {
			acoDoor = coaco.Item(i);
			if (minZ != 0 && acoDoor.maploc.z < (minZ - 0.1)) {
			//	core.debug("Door is too low. (" + Math.abs(acoDoor.maploc.z - minZ) + ")");
				coaco.RemoveIitem(i);
				continue;
			} else if (maxZ != 0 && acoDoor.maploc.z > (maxZ + 0.1)) {
			//	core.debug("Door is too high. (" + Math.abs(acoDoor.maploc.z - maxZ) + ")");
				coaco.RemoveIitem(i);
				continue;
			}
			
			//closestSpot = core.getClosestSpot(acoDoor.lng acoDoor.lat, maplocA.lng, maplocA.lat, maplocB.lng, maplocB.lat);
			closestSpot = core.getClosestSpotOnMaplocLine(maplocA, maplocB, acoDoor.maploc);
			dist = core.getLineLength(closestSpot.x, closestSpot.y, acoDoor.maploc.lng, acoDoor.maploc.lat);
			
			if (dist > maxDist) {
				coaco.RemoveIitem(i);
			}
		}
		return coaco;
	};
	
	/*
		strikeKeySync: Strike a key.
	*/
	core.strikeKeySync = function strikeKeySync(key, duration) {
		core.debug("<strikeKeySync> key: " + key + ", duration: " + duration);
		if (!duration) {
			duration = 0;
		}
		if (skapi.fInChatBuffer) {
			core.debug("   In chat area.  Deselecting.");
			skapi.Keys("{shift}", kmoUp);	// Try to unshift
			skapi.Keys("{ESC}");
		}
		if (!duration) {
			skapi.Keys(key);
		} else {
			skapi.Keys(key, kmoDown);
			
			//skapi.Sleep(duration);
			skapi.WaitEvent(duration, wemFullTimeout);
			skapi.Keys(key, kmoUp);
		}
	};

	core.amMoving = function amMoving() {
		return animationState.running || animationState.walkingForward;
	};

	var lastJump = 0;
	
	/*
		jump: Jump only if it's been 2 seconds from our last jump.
	*/
	core.jump = function jump() {
		if ((new Date()).getTime() - lastJump > 2000) {
			core.strikeKeySync(cmidJump, 250);
			lastJump = (new Date()).getTime();
		}
	};

	var animationState = core.animationState = {
		running         : false,
		walkingForward  : false,
		walkingBackwards: false,
		turningLeft     : false,
		turningRight    : false,
		time            : 0
	};

	/*
		onAnimation: Main animation monitoring function to keep track of what we're doing.
	*/
	function onAnimationGlobal(payload) {
		if (skapi.acoChar && skapi.acoChar.oid == payload.object) {
			var time = animationState.time;
			
			if (payload.activity == 1 && payload.flags == 0) {
				core.debug("0x791e We've stopped moving");
				animationState.running = false;
				animationState.walkingForward = false;
				animationState.walkingBackwards = false;
				animationState.turningLeft = false;
				animationState.turningRight = false;
				animationState.time = new Date().getTime();
			}
			
			if (payload.activity == 1 && payload.animation_1 == 7) {
				core.debug("0x5e93 We're running forward");
				animationState.running = true;
				animationState.walkingForward = false;
				animationState.time = new Date().getTime();
			}
			
			if (payload.activity == 1 && payload.animation_1 == 5) {
				animationState.running = false;
				if (typeof payload.float_1 !== "undefined" && payload.float_1 < 0) {
					core.debug("0xd45b We're walking backwards");
					animationState.walkingBackwards = true;
					animationState.walkingForward = false;
				} else {
					core.debug("0x8acb We're walking forward");
					animationState.walkingForward = true;
					animationState.walkingBackwards = false;
				}
				animationState.time = new Date().getTime();
			}

			if (payload.activity == 1 && payload.animation_3 == 13) {
				animationState.turningLeft = false;
				animationState.turningRight = false;

				if (payload.float_3 < 0) {
					core.debug("0x0ce2 We're turning left");
					animationState.turningLeft = true;
				} else {
					core.debug("0xf43e We're turning right");
					animationState.turningRight = true;
				}
				
				//core.console(" What direction? " + JSON.stringify(payload,null,"\t"));
				animationState.time = new Date().getTime();
			}

			if (payload.activity == 1 && payload.animation_2 == 15) {
				core.debug("0x74dc We're side stepping");
				animationState.turningLeft = false;
				animationState.turningRight = false;
				animationState.running = false;
				animationState.walkingForward = false;
				animationState.time = new Date().getTime();
			}
			
			if (payload.animation_type == AnimationFilter.animationTypes.TurnToObject) {
				core.debug("0x3f77 We've turning to a object.");
				animationState.running = false;
				animationState.walkingForward = false;
				animationState.walkingBackwards = false;
				animationState.turningLeft = false;
				animationState.turningRight = false;
				animationState.time = new Date().getTime();
			}

			if (time == animationState.time) {
				core.console("<onAnimationGlobal> uncaught " + JSON.stringify(payload, null, "\t"));
			}
			return;
		}
		
		var acoObject = skapi.AcoFromOid(payload.object);
		if (acoObject.oty & otyDoor) {
			if (payload.animation_1 == 11) {
				core.openedDoors[payload.object] = new Date().getTime();
				return;
			} else if (payload.animation_1 == 12) {
				if (core.openedDoors[payload.object]) {
					core.debug("Door as closed " + (new Date().getTime() - core.openedDoors[payload.object]) + "ms after it was opened.");
				}
				delete core.openedDoors[payload.object];
			}
		}
	}
	
	core.dist2DToMaploc = function dist2DToMaploc(maploc) {
		core.debug("<dist2DToMaploc> " + (maploc && maploc.sz(3)));
		return skapi.maplocCur.Dist2DToMaploc(maploc);
	};
	
	/*
		getETA: Get the optimal travel ETA from our location to a maploc. Our actual travel time is usually a few seconds longer.
	*/
	core.getETA = function getETA(maploc, radius) {
		var distance = core.dist2DToMaploc(maploc);
		if (typeof radius !== "undefined") {
			distance -= radius;
			distance = Math.max(0, distance);
		}
		return core.round((distance / skapi.vRun) * 1000);
	};

	/*
		isPortal: Return true/false if a object is a portal. Some portals are special and don't match mcmPortal or ocmPortal. These are the other contitions they do match.
	*/
	core.isPortal = function isPortal(aco) {
		if (aco.icon != 4203) {
			return false;
		}
		
		if (aco.olc != 0x8) {
			return false;
		}
		
		if (aco.eqm != 0) {
			return false;
		}
		
		if (aco.material != 0) {
			return false;
		}
		
		if (aco.burden != 0) {
			return false;
		}

		return true;
	};

	/*
		travelRouteSync: Travel a legacy Skunknav route using our nav engine. 
	core.travelRouteSync = function travelRouteSync(route, arriveDist, stopOnArrival, cmsecTimeout, fWalk) {
		core.debug("<travelRouteSync> arriveDist: " + arriveDist + ", stopOnArrival: " + stopOnArrival + ", cmsecTimeout: " + cmsecTimeout + ", fWalk: " + fWalk);
		if (typeof cmsecTimeout === "undefined") cmsecTimeout = defaultTimeout;
		if (typeof fWalk === "undefined") fWalk = false;

		var result;
		
		var pmaploc;
		var maploc;
		
		var startTime = new Date();
		var elapsed;
		var remaining;
		for (var i = 0; i < route.cmaploc ; i++) {
			elapsed = new Date() - startTime;
			remaining = cmsecTimeout - elapsed;
			core.debug("elapsed: " + elapsed + ", remaining: " + remaining);
			if (typeof cmsecTimeout !== "undefined") {
				if (remaining <= 0) {
					core.debug("Timeout reached.");
					break;
				}
			}

			maploc = route.MaplocGet(i);
			if (!maploc) {
				continue;
			}
			
			if (FMaplocIsDoor(maploc)) {
				core.debug("Skipping door waypoint...");
				continue;
			} else if (FMaplocIsPortal(maploc)) {
				var acoPortal = AcoFindNearestSz(maploc.maploc, maploc.szAco, maploc.oidHint, otyPortal);
				core.info("Using portal (" + (acoPortal && acoPortal.oid) + ")...");
				if (acoPortal) {
					//skapi.SelectAco(acoPortal);
					//UsePortalSel();
					core.usePortalAcoSync(acoPortal);
					continue;
				} else {
					core.info("Can't find portal!");
					break;
				}
			}

			distance = dist2DToMaploc(maploc);
			if (distance < arriveDist) {
				continue;
			}
			
			core.console(i+": Traveling to " + maploc.sz(3) + " " + core.round(meters(distance)) + "m away, ETA: " + getETA(maploc) + "ms");

			result = goToMaplocSync({
				maploc          : maploc, 
				distArrive      : distArrive,
				walk            : fWalk,
				fStopOnArrival  : (i == route.cmaploc-1 && stopOnArrival),
				timeout         : remaining,
				startingMaploc  : pmaploc
			});

			if (!(result & flags.success)) {
				core.debug("We've failed to reach waypoint " + maploc);
				break;
			}
			
			//goToMaplocSync(maploc, distArrive, fWalk, fStopOnArrival, cmsecTimeout, startingMaploc)
			
			pmaploc = maploc;
		}
			
		return result;
	};
	*/
	
	var Route;
	(function() {
		/*
			Route: Our fake Skunknav route object as a drop in replacement for Skunknav.
		*/
		
		Route = function(skunkRoute) {
			this.skunkRoute = skunkRoute;
		};
		
		Route.prototype.Go = function Go(arriveDist, stopOnArrival, cmsecTimeout, fWalk) {
			if (typeof this.csecTimeout !== "undefined") {
			//	core.debug("Setting csecTimeout to " + this.csecTimeout);
				this.skunkRoute.csecTimeout = this.csecTimeout;
			} else {
				this.skunkRoute.csecTimeout = routeTimeout;
			}
			
			if (typeof arriveDist === "undefined")  {
				arriveDist = distArriveMin;
			}
			
			if (typeof stopOnArrival === "undefined")  {
				stopOnArrival = true;
			}
			if (typeof fWalk === "undefined") fWalk = false;
			
			return core.travelRouteSync(this.skunkRoute, arriveDist, stopOnArrival, cmsecTimeout, fWalk);
		};

		Route.prototype.RouteReverse = function RouteReverse() {
			this.skunkRoute = this.skunkRoute.RouteReverse();
		};
		
		Route.prototype.GetRawRoute = function GetRawRoute() {
			return this.skunkRoute;
		};
		
		Route.prototype.GetFirstMaploc = function GetFirstMaploc() {
			return this.skunkRoute.MaplocGet(0);
		};
		
		Route.prototype.GetLastMaploc = function GetLastMaploc() {
			return this.skunkRoute.MaplocGet(this.skunkRoute.cmaploc - 1);
		};
		
		Route.prototype.GetMaploc = function GetMaploc(index) {
			return this.skunkRoute.MaplocGet(index);
		};
		
		Route.prototype.GetMaplocCount = function getMaplocCount() {
			return this.skunkRoute.cmaploc;
		};
	})();
	
	/*
		routeFromFile: Load a SkunkNav route from file.
	*/
	core.routeFromFile = function routeFromFile(szRoute) {
		// TODO: Replace RouteFromFile (from SkunkNav.js) with our own function.
		// eslint-disable-next-line no-undef
		var skunkRoute = RouteFromFile(szRoute);
		core.debug("<routeFromFile> " + szRoute + ", " + (typeof skunkRoute));
		return new Route(skunkRoute);
	};

	core.clearFootprints = function clearFootprints() {
		core.debug("<clearFootprints>");
		footprints.splice(0, footprints.length);
	};

	core.purgeFootprintsByTime = function purgeFootprintsByTime(elapsed) {
		// eslint-disable-next-line no-restricted-syntax
		for (var i = footprints.length - 1; i > 0; i--) {
			if ((new Date()).getTime() - footprints[i].time > elapsed) {
				footprints.splice(i, 1);
			}
		}
	};
	
	/*
		purgeFootprintsByDist: Purge past footprints past a certain distance.
	*/
	core.purgeFootprintsByDist = function purgeFootprintsByDist(dist, stuckStartedPoint) {
		if (typeof stuckStartedPoint === "undefined") stuckStartedPoint = skapi.maplocCur;
		var purged = 0;
		var d;
		// eslint-disable-next-line no-restricted-syntax
		for (var i = footprints.length - 1; i > 0; i--) {
			d = stuckStartedPoint.Dist2DToMaploc(footprints[i].maploc);
			if (d > dist) {
			//	core.info("Puring footprint: " + i + " (" + core.round(d*240,1) + "m)");
				purged += 1;
				footprints.splice(i, 1);
			}
		}
		if (purged > 0) {
			core.debug("Purged " + purged + " footprint past " + core.round(dist * 240, 1) + "m.");
		}
	};

	/*
		degrees: Convert radians to degrees.
	*/
	core.degrees = function degrees(rad) {
		return rad * (180 / Math.PI);
	};
	
	/*
		radians: Convert degrees to radians.
	*/
	core.radians = function radians(deg) {
		return deg * (Math.PI / 180);
	};
	
	//var autoRunning = false;
	core.OnTipMessage = function OnTipMessage(szMsg) {
		if (szMsg.match("AutoRun ON")) {
			core.debug("<OnTipMessage> " + szMsg);
			
			//autoRunning = true;
		} else if (szMsg.match("AutoRun OFF")) {
			core.debug("<OnTipMessage> " + szMsg);
			
			//autoRunning = false;
		}
	};
	
	core.OnStartPortalSelf = function OnStartPortalSelf() {
		core.debug("<OnStartPortalSelf>, assuming we're not moving anymore.");
		animationState.running           = false;
		animationState.walkingForward = false;
		
		//autoRunning         = false;
		
		skapi.Keys(cmidWalkForward, kmoUp);
		if (skapi.chop & chopRunAsDefault) skapi.Keys(cmidMovementWalkMode, kmoUp);
	};
	
	/*
		OnTimer: Timer to record where we've been to be used with stuckness detection.
	*/
	core.OnTimer = function OnTimer(t) {
		// Record our previous locations to track if we get stuck.
		//core.debug("<OnTimer>");
		if (t.tag == footstepTimer.tag) {
			footprints.splice(0, 0, {
				maploc: skapi.maplocCur,
				time  : (new Date()).getTime()
			});
			
			//footprints.splice((1000*60/500), footprints.length);	// Roughly 60 seconds.
			core.purgeFootprintsByTime(1000 * 60);
				
			//	core.debug("footprints: " + footprints.length + ", " + (new Date() - footprints[footprints.length-1].time) + ", " + (skapi.maplocCur.Dist2DToMaploc(footprints[footprints.length-1].maploc)));
			t.cmsec = -1000;
		}
	};

	/**
	 * applyMethod() Apply a function with a thisArg object and argument. 
	 *
	 * @param {function} method - Function to be called.
	 * @param {object} thisArg - Object to be the 'this' object in the function.
	 * @param {array} args - List of arguments for the function.
	 * @returns {variable} Whatever the method returns.
	 */
	core.applyMethod = function applyMethod(method, thisArg, args) {
		return method.apply(thisArg, args);
	};
	
	core.timedOut = function timedOut(resolve) {
		resolve(new core.Error("TIMED_OUT"));
	};

	core.strikeKey = function strikeKey(params, callback) {
		var key = params.key;
		var duration = params.duration || 0;
		
		// Check if user's typing in chat, escape out of that first.
		if (skapi.fInChatBuffer) {
			core.debug("   In chat area.  Deselecting.");
			skapi.Keys("{shift}", kmoUp);	// Try to unshift so we don't log off.
			skapi.Keys("{ESC}");
		}
		
		skapi.Keys(key, kmoDown);
		core.setTimeout(function next() {
			skapi.Keys(key, kmoUp);
			callback();
		}, duration);
	};

	/////////////////

	core.face = function face(params, callback) {
		core.debug("<face>");
		
		var heading = params.heading;
		var variance = params.variance || defaultFaceVariance;
		var timeout = params.timeout || 5000;
		
		function resolve(err, result) {
			core.debug("<face|resolve>", err, result);
			if (tid) clearTimeout(tid);
			if (tid2) clearTimeout(tid2);
			skapi.Keys(cmidDirection, kmoUp);
			callback && core.setImmediate(core.applyMethod, callback, params && params.thisArg, arguments); // Call callback in a timer so other event handlers can process the event.
		}

		var rHeading = core.headingTurn({heading: heading});
		
		if (Math.abs(rHeading) <= variance) {
			return resolve(undefined, true); // {success: true}
		}

		var cmidDirection = cmidMovementTurnRight;
		if (rHeading < 0) {
			cmidDirection = cmidMovementTurnLeft;
		}

		function tick() {
			var crHead = core.headingTurn({heading: heading});
			if (Math.abs(crHead) <= variance) {
				return resolve(undefined, true);
			}
		}

		var tid2 = core.setInterval(tick, 1);
		var tid = core.setTimeout(core.timedOut, timeout, resolve);
		skapi.Keys(cmidDirection, kmoDown);
	};
	
	core.faceSync = function factory() {
		return function faceSync(params) {
			core.debug("<faceSync>");
			skapi.WaitEvent(); // flush
			var results;
			core.face(params, function onResult(err, result) {
				core.debug("<faceSync|onResult>", err, result);
				results = Array.prototype.slice.call(arguments);
			});
			do {
				skapi.WaitEvent(100, wemSingle);
			} while (typeof results === "undefined");
			var err = results && results.length > 0 && results[0];
			if (err) throw err;
			return results && results.length > 1 && results[1];
		};
	}();
	
	core.isOpen = function factory() {
		return function isOpen(params, callback) {
			core.debug("<isOpen>");
			var aco = params.aco;
			var timeout = params.timeout || 1000;
			
			function resolve(err, result) {
				core.debug("<face|resolve>", err, result);
				if (tid) clearTimeout(tid);
				handler &&  skapi.RemoveHandler(evidNil, handler);
				callback && core.setImmediate(core.applyMethod, callback, params && params.thisArg, arguments); // Call callback in a timer so other event handlers can process the event.
			}
			
			var handler = {};
			handler.OnAssessItem = function OnAssessItem(aco2, fSuccess, ibi) { // , iai, iwi, iei, ipi
				if (aco.oid == aco2.oid) {
					resolve(undefined, ibi.fOpen);
				}
			};
			skapi.AddHandler(evidOnAssessItem, handler);

			var tid = core.setTimeout(core.timedOut, timeout, resolve);
			
			skapi.AssessAco(aco);
		};
	}();

	core.isOpenSync = function factory() {
		return function isOpenSync(params) {
			core.debug("<isOpenSync>");
			skapi.WaitEvent(); // flush
			var results;
			core.isOpen(params, function onResult(err, result) {
				core.debug("<isOpenSync|onResult>", err, result);
				results = Array.prototype.slice.call(arguments);
			});
			do {
				skapi.WaitEvent(100, wemSingle);
			} while (typeof results === "undefined");
			var err = results && results.length > 0 && results[0];
			if (err) throw err;
			return results && results.length > 1 && results[1];
		};
	}();
	
	core.openDoor = function factory() {
		return function openDoor(params, callback) {
			core.debug("<openDoor>");
			var aco = params.aco;
			var timeout = params.timeout || 3000;

			function resolve(err, result) {
				core.debug("<face|resolve>", err, result);
				tid && clearTimeout(tid);
				handler &&  skapi.RemoveHandler(evidNil, handler);
				AnimationFilter.removeCallback(onAnimation);
				callback && core.setImmediate(core.applyMethod, callback, params && params.thisArg, arguments); // Call callback in a timer so other event handlers can process the event.
			}
			
			function onAnimation(payload) {
				if (payload.object != aco.oid) return;
				if (payload.animation_1 == 11) {
					return resolve(undefined, true);
				} else if (payload.animation_1 == 12) {
					return resolve(new core.Error("DOOR_CLOSED"));
				}
				
				core.debug("<openDoor|onAnimation> UNCAUGHT " + JSON.stringify(payload, null, "\t"));
			}
			AnimationFilter.addCallback(onAnimation);
			
			var handler = {};
			handler.OnChatServer = function OnChatServer(szMsg) {
				if (szMsg.match(/This door is already closed!/)) {
					return resolve(new core.Error("DOOR_CLOSED"));
				}
			};
			skapi.AddHandler(evidOnChatServer, handler);
			
			var tid = core.setTimeout(core.timedOut, timeout, resolve);
			
			aco.Use();
		};
	}();

	core.openDoorSync = function factory() {
		return function openDoorSync(params) {
			core.debug("<openDoorSync>");
			skapi.WaitEvent(); // flush
			var results;
			core.openDoor(params, function onResult(err, result) {
				core.debug("<openDoorSync|onResult>", err, result);
				results = Array.prototype.slice.call(arguments);
			});
			do {
				skapi.WaitEvent(100, wemSingle);
			} while (typeof results === "undefined");
			var err = results && results.length > 0 && results[0];
			if (err) throw err;
			return results && results.length > 1 && results[1];
		};
	}();
	
	core.startWalking = function factory() {
		return function startWalking(params, callback) {
			core.debug("<startWalking>");
			var timeout = params && params.timeout || 1000;

			function resolve(err, result) {
				core.debug("<startWalking|resolve>", err, result);
				tid && clearTimeout(tid);
				AnimationFilter.removeCallback(onAnimation);
				callback && core.setImmediate(core.applyMethod, callback, params && params.thisArg, arguments); // Call callback in a timer so other event handlers can process the event.
			}

			function onAnimation(payload) {
				if (payload.object != skapi.acoChar.oid) return;
				if (payload.activity == 1 && payload.animation_type == 0 && payload.type_flags == 0) {
					if (payload.flags == 0) {// full stop.
						return resolve(new Error("STOPPED"));
					} else if (payload.animation_1 == 5) { //walking
						return resolve(null, true);
					} else if (payload.animation_1 == 7) { //running
						return resolve(new core.Error("STARTED_RUNNING"));
					}
				}
				
				core.console("<startWalking|onAnimation> Uncaught " + JSON.stringify(payload, null, "\t"));
			}
			AnimationFilter.addCallback(onAnimation);

			if (skapi.chop & chopRunAsDefault) {
				// Check if run as default is enabled. If so, hold down walk key.
				skapi.Keys(cmidMovementWalkMode, kmoUp);
			}
			skapi.Keys(cmidWalkForward, kmoUp);
			skapi.Sleep(100);
			
			if (skapi.chop & chopRunAsDefault) {
				// Check if run as default is enabled. If so, hold down walk key.
				skapi.Keys(cmidMovementWalkMode, kmoDown);
				skapi.Sleep(100);
			}
			
			skapi.Keys(cmidWalkForward, kmoDown);

			var tid = core.setTimeout(core.timedOut, timeout, resolve);
		};
	}();
	
	core.startWalkingSync = function factory() {
		return function startWalkingSync(params) {
			core.debug("<startWalkingSync>");
			skapi.WaitEvent(); // flush
			var results;
			core.startWalking(params, function onResult(err, result) {
				core.debug("<startWalkingSync|onResult>", err, result);
				results = Array.prototype.slice.call(arguments);
			});
			do {
				skapi.WaitEvent(100, wemSingle);
			} while (typeof results === "undefined");
			var err = results && results.length > 0 && results[0];
			if (err) throw err;
			return results && results.length > 1 && results[1];
		};
	}();
	
	core.startAutoRunning = function factory() {
		return function startAutoRunning(params, callback) {
			core.debug("<startAutoRunning>");
			params = params || {};
			var timeout = params.timeout || 1000;

			function resolve(err, result) {
				core.debug("<startAutoRunning|resolve>", err, result);
				tid && clearTimeout(tid);
				tid2 && clearTimeout(tid2);
				
				//AnimationFilter.removeCallback(onAnimation);
				callback && core.setImmediate(core.applyMethod, callback, params && params.thisArg, arguments); // Call callback in a timer so other event handlers can process the event.
			}

			/*
			function onAnimation(payload) {
				if (payload.object != skapi.acoChar.oid) return;
				if (payload.activity == 1 && payload.animation_type == 0 && payload.type_flags == 0) {
					if (payload.flags == 0) { // full stop.
						return resolve(new Error("STOPPED"));
					} else {
						if (typeof payload.animation_1 !== "undefined" && payload.animation_1 == 7) { //running
							return resolve(undefined, true);
						}
					}
				}
				//core.console("<startAutoRunning|onAnimation> Uncaught " + JSON.stringify(payload,null,"\t"));
			};
			AnimationFilter.addCallback(onAnimation);
			*/
			
			var before = core.clone(animationState);
			function tick() {
				if (animationState.running == true) {
					return resolve(undefined, true);
				}
				if (before.time != animationState.time) {
					var diff = core.difference([before, animationState]);
					core.debug("0xa47e Got a animation but not running", JSON.stringify(diff));
					
					//return resolve(new core.Error("FAILED"));
				}
			}
			var tid2 = core.setInterval(tick, 100);

			core.strikeKeySync(cmidAutoRun);
			var tid = core.setTimeout(core.timedOut, timeout, resolve);
		};
	}();

	core.startAutoRunningSync = function factory() {
		return function startAutoRunningSync(params) {
			core.debug("<startAutoRunningSync>");
			skapi.WaitEvent(); // flush
			var results;
			core.startAutoRunning(params, function onResult(err, result) {
				core.debug("<startAutoRunningSync|onResult>", err, result);
				results = Array.prototype.slice.call(arguments);
			});
			do {
				skapi.WaitEvent(100, wemSingle);
			} while (typeof results === "undefined");
			var err = results && results.length > 0 && results[0];
			if (err) throw err;
			return results && results.length > 1 && results[1];
		};
	}();

	core.strikeReadyKey = function factory() {
		return function strikeReadyKey(params, callback) {
			core.debug("<strikeReadyKey>");
			params = params || {};
			var timeout = params.timeout || 1000;
			
			function resolve(err, result) {
				core.debug("<strikeReadyKey|resolve>", err, result);
				tid && clearTimeout(tid);
				
				//handler &&  skapi.RemoveHandler(evidNil, handler);
				AnimationFilter.removeCallback(onAnimation);
				callback && core.setImmediate(core.applyMethod, callback, params && params.thisArg, arguments); // Call callback in a timer so other event handlers can process the event.
			}

			function onAnimation(payload) {
				if (payload.object != skapi.acoChar.oid) return;
				if (payload.activity == 1 && payload.animation_type == 0 && payload.type_flags == 0) {
					if (payload.flags == 0) { // full stop.
						return resolve(undefined, true);
					}
				}
				
				//core.debug("<strikeReadyKey|onAnimation> Uncaught " + JSON.stringify(payload, null, "\t"));
			}
			AnimationFilter.addCallback(onAnimation);
			
			core.strikeKeySync(cmidReady); 
			
			var tid = core.setTimeout(core.timedOut, timeout, resolve);
		};
	}();

	core.strikeReadyKeySync = function factory() {
		return function strikeReadyKeySync(params) {
			core.debug("<strikeReadyKeySync>");
			skapi.WaitEvent(); // flush
			var results;
			core.strikeReadyKey(params, function onResult(err, result) {
				core.debug("<strikeReadyKeySync|onResult>", err, result);
				results = Array.prototype.slice.call(arguments);
			});
			do {
				skapi.WaitEvent(100, wemSingle);
			} while (typeof results === "undefined");
			var err = results && results.length > 0 && results[0];
			if (err) throw err;
			return results && results.length > 1 && results[1];
		};
	}();

	core.stopAutoRunning = function factory() {
		return function stopAutoRunning(params, callback) {
			core.debug("<stopAutoRunning>");
			var timeout = params && params.timeout || 1000;
			
			function resolve() {
				if (tid) clearTimeout(tid);
				
				//if (handler) skapi.RemoveHandler(evidNil, handler);
				AnimationFilter.removeCallback(onAnimation);
				callback && core.setImmediate(core.applyMethod, callback, params && params.thisArg, arguments); // Call callback in a timer so other event handlers can process the event.
			}

			function onAnimation(payload) {
				if (payload.object != skapi.acoChar.oid) return;
				if (payload.activity == 1 && payload.animation_type == 0 && payload.type_flags == 0) {
					if (payload.flags == 0) { // full stop.
						return resolve(null, true);
					}
					if (typeof payload.animation_1 !== "undefined" && payload.animation_1 == 7) { //running
						return resolve(new Error("STARTED_RUNNING"));
					}
					if (payload.flags == 1) {
						return resolve(null, true);
					}
				}
			}
			AnimationFilter.addCallback(onAnimation);
			
			skapi.Keys(cmidAutoRun);
			
			var tid = core.setTimeout(core.timedOut, timeout, resolve);
		};
	}();

	core.stopAutoRunningSync = function factory() {
		return function stopAutoRunningSync(params) {
			core.debug("<stopAutoRunningSync>");
			skapi.WaitEvent(); // flush
			var results;
			core.stopAutoRunning(params, function onResult(err, result) {
				core.debug("<stopAutoRunningSync|onResult>", err, result);
				results = Array.prototype.slice.call(arguments);
			});
			do {
				skapi.WaitEvent(100, wemSingle);
			} while (typeof results === "undefined");
			var err = results && results.length > 0 && results[0];
			if (err) throw err;
			return results && results.length > 1 && results[1];
		};
	}();

	core.waitToExitPortalSpace = function factory() {
		return function waitToExitPortalSpace(params, callback) {
			core.debug("<waitToExitPortalSpace>");
			var timeout = params && params.timeout || 1000 * 30;
			
			function resolve() {
				if (tid) clearTimeout(tid);
				if (handler) skapi.RemoveHandler(evidNil, handler);
				MaterializeFilter.removeCallback(onMaterialize);
				callback && core.setImmediate(core.applyMethod, callback, params && params.thisArg, arguments); // Call callback in a timer so other event handlers can process the event.
			}

			var handler = {};
			handler.OnEndPortalSelf = function OnEndPortalSelf() {
				core.debug("<waitToExitPortalSpace|OnEndPortalSelf>");
				resolve(null, true);
			};
			skapi.AddHandler(evidOnEndPortalSelf, handler);

			function onMaterialize() {
				core.debug("<waitToExitPortalSpace|onMaterialize>");
				resolve(null, true);
			}
			MaterializeFilter.addCallback(onMaterialize);
			
			var tid = core.setTimeout(core.timedOut, timeout, resolve);
		};
	}();

	core.waitToExitPortalSpaceSync = function factory() {
		return function waitToExitPortalSpaceSync(params) {
			core.debug("<waitToExitPortalSpaceSync>");
			skapi.WaitEvent(); // flush
			var results;
			core.waitToExitPortalSpace(params, function onResult(err, result) {
				core.debug("<waitToExitPortalSpaceSync|onResult>", err, result);
				results = Array.prototype.slice.call(arguments);
			});
			do {
				skapi.WaitEvent(100, wemSingle);
			} while (typeof results === "undefined");
			var err = results && results.length > 0 && results[0];
			if (err) throw err;
			return results && results.length > 1 && results[1];
		};
	}();

	core.usePortal = function factory() {
		return function usePortal(params, callback) {
			core.debug("<usePortal>");
			var aco = params.aco;
			var timeout = params && params.timeout || 1000 * 10;
			
			function resolve() {
				if (tid) clearTimeout(tid);
				if (handler) skapi.RemoveHandler(evidNil, handler);
				callback && core.setImmediate(core.applyMethod, callback, params && params.thisArg, arguments); // Call callback in a timer so other event handlers can process the event.
			}

			var handler = {};
			handler.OnTipMessage = function OnTipMessage(szMsg) {
				// Watch for Moved too far message.
				if (szMsg.match(/^You have moved too far/)) {
					core.debug("<usePortal><OnTipMessage> szMsg: '" + szMsg + "'");
					return resolve(new Error("MOVED_TOO_FAR"));
				} else if (szMsg.match(/^You're too busy/)) {
					core.debug("<usePortal><OnTipMessage> szMsg: '" + szMsg + "'");
					return resolve(new Error("TOO_BUSY"));
				} else if (szMsg.match(/^Unable to move to object!/)) {
					//Debug("<usePortal><OnTipMessage> szMsg: '" + szMsg + "'");
					return resolve(new Error("UNABLE_TO_MOVE_TO_OBJECT"));
				} else if (szMsg.match(/Using the (.*)/)) {
					return;
				} else if (szMsg.match(/In Portal Space - Please Wait.../)) {
					core.debug("<usePortal><OnTipMessage> szMsg: '" + szMsg + "'");
					return resolve(new Error("IN_PORTAL_SPACE"));
				}
				core.debug("<usePortal><OnTipMessage> UNCAUGHT szMsg: '" + szMsg + "'");
			};
			skapi.AddHandler(evidOnTipMessage, handler);
			
			handler.OnStartPortalSelf = function OnStartPortalSelf() {
				resolve(null, true);
			};
			skapi.AddHandler(evidOnStartPortalSelf, handler);

			aco.Use();
			
			var tid = core.setTimeout(core.timedOut, timeout, resolve);
		};
	}();

	core.usePortalSync = function factory() {
		return function usePortalSync(params) {
			core.debug("<usePortalSync>");
			skapi.WaitEvent(); // flush
			var results;
			core.usePortal(params, function onResult(err, result) {
				core.debug("<usePortalSync|onResult>", err, result);
				results = Array.prototype.slice.call(arguments);
			});
			do {
				skapi.WaitEvent(100, wemSingle);
			} while (typeof results === "undefined");
			var err = results && results.length > 0 && results[0];
			if (err) throw err;
			return results && results.length > 1 && results[1];
		};
	}();

	core.stopWalking = function factory() {
		return function stopWalking(params, callback) {
			core.debug("<stopWalking>");
			
			var timeout = params && params.timeout || 1000;
			
			function resolve() {
				if (tid) clearTimeout(tid);
				
				//if (handler) skapi.RemoveHandler(evidNil, handler);
				AnimationFilter.removeCallback(onAnimation);
				callback && core.setImmediate(core.applyMethod, callback, params && params.thisArg, arguments); // Call callback in a timer so other event handlers can process the event.
			}

			function onAnimation(payload) {
				if (payload.object != skapi.acoChar.oid) return;
				if (payload.activity == 1 && payload.animation_type == 0 && payload.type_flags == 0) {
					if (payload.flags == 0) { // full stop.
						resolve(null, true);
					} else {
						if (typeof payload.animation_1 !== "undefined") { //running/walking
							return resolve(new Error("FAILED"));
						}
						if (payload.flags == 1) {
							resolve(null, true);
						}
					}
				}
			}
			AnimationFilter.addCallback(onAnimation);
			
			skapi.Keys(cmidWalkForward, kmoUp);
			skapi.Keys(cmidReady);
			
			var tid = core.setTimeout(core.timedOut, timeout, resolve);
		};
	}();

	core.stopWalkingSync = function factory() {
		return function stopWalkingSync(params) {
			core.debug("<stopWalkingSync>");
			skapi.WaitEvent(); // flush
			var results;
			core.stopWalking(params, function onResult(err, result) {
				core.debug("<stopWalkingSync|onResult>", err, result);
				results = Array.prototype.slice.call(arguments);
			});
			do {
				skapi.WaitEvent(100, wemSingle);
			} while (typeof results === "undefined");
			var err = results && results.length > 0 && results[0];
			if (err) throw err;
			return results && results.length > 1 && results[1];
		};
	}();

	core.goToMaploc = function factory() {
		return function goToMaploc(params, callback) {
			core.debug("<goToMaploc>");
			
			var maploc          = params.maploc;
			var aco             = params.aco;
			var distArrive      = params.distArrive;
			var walk            = params.walk;
			var stopOnArrival   = params.stopOnArrival;
			var timeout         = params.timeout;
			var startingMaploc  = params.startingMaploc;
			var avoidPortals    = true;
			if (typeof params.avoidPortals !== "undefined") avoidPortals = params.avoidPortals;
			
			var cancellation = params.cancellation || new core.Cancellation();
			
			var resolve = function _resolve() {
				core.debug("<goToMaploc|resolve>");
				
				//if (tid2) clearTimeout(tid2);

				if (stopOnArrival == true) {
					core.debug("Full Stop!");
					
					try {
						core.strikeReadyKeySync();
					} catch (err) {
						core.console("0xa35a Error while striking ready key", err);
						
						//throw err;
					}
					
					
				}
				
				callback && core.setImmediate(core.applyMethod, callback, params && params.thisArg, arguments); // Call callback in a timer so other event handlers can process the event.
				resolve = undefined;
			};
			
			
			var course = new Course();
			
			if (typeof aco !== "undefined") {
				course.setAco(aco);
			} else {
				course.setMaploc(maploc);
			}

			if (typeof distArrive !== "undefined") {
				course.setDistArrive(distArrive);
			}
			if (typeof timeout !== "undefined") {
				course.setTimeout(timeout);
			}
			if (typeof stopOnArrival === "undefined") {
				stopOnArrival = true;
			}
			if (typeof startingMaploc !== "undefined") {
				course.setStartingMaploc(startingMaploc);
			}
			
			if (typeof walk !== "undefined") {
				course.setWalk(walk);
			}
			
			course.setAvoidPortals(avoidPortals);
			
			//course.setZigZag(true);

			course.start();

			function tick() {
				if (cancellation && cancellation.canceled) return resolve(new core.Error("CANCELLATION"));
				if (course.hasArrived()) {
					core.info("Arrived at " + maploc);
					return resolve(null, true);
				}
				
				if (course.hasTimedout()) {
					return resolve(new core.Error("TIMED_OUT"));
				}

				try {
					var started = new Date().getTime();
					course.tickSync();
					var elapsed = new Date().getTime() - started;
					core.debug("Took " + elapsed + "ms to tick.");
				} catch (err) {
					core.console("0x873d Error while ticking course.", err);
					
					//throw err;
				}
				core.setTimeout(tick, 250);
				
				
				/*
				var started = new Date().getTime();
				course.tick(undefined, function onCourseTick(err, results) {
					var elapsed = new Date().getTime() - started;
					core.debug("<onCourseTick>", err, results);
					//core.info("Took " + elapsed + "ms to do course tick.");
					
					
					// Queue again, since tickSync() calls waitevent, we don't want to use setInterval.
					core.setTimeout(tick, 250);
				});
				*/
			}
			core.setImmediate(tick, 250);
			
		};
	}();
	
	core.goToMaplocSync = function factory() {
		return function goToMaplocSync(params) {
			core.debug("<goToMaplocSync>");
			skapi.WaitEvent(); // flush
			var results;
			core.goToMaploc(params, function onResult(err, result) {
				core.debug("<goToMaplocSync|onResult>", err, result);
				results = Array.prototype.slice.call(arguments);
			});
			do {
				skapi.WaitEvent(100, wemSingle);
			} while (typeof results === "undefined");
			var err = results && results.length > 0 && results[0];
			if (err) throw err;
			return results && results.length > 1 && results[1];
		};
	}();

	/*
		startMoving: start moving forward, auto decides whether to walk or run.
	*/
	core.startMoving = function factory() {
		return function startMoving(params, callback) {
			core.debug("<startMoving>");
			var forceWalk = (params.forceWalk === undefined ? false : params.forceWalk);
			var maploc = params.maploc;

			if (forceWalk == true) {
				return core.startWalking(undefined, callback);
			}
			
			// If no maploc provided, just start running.
			if (!maploc) {
				return core.startAutoRunning(undefined, callback);
			}
			
			var dist = skapi.maplocCur.Dist2DToMaploc(maploc);
			if (dist <= distWalk) {
				return core.startWalking(undefined, callback);
			} else {
				return core.startAutoRunning(undefined, callback);
			}
		};
	}();
	
	core.startMovingSync = function factory() {
		return function startMovingSync(params) {
			core.debug("<startMovingSync>");
			skapi.WaitEvent(); // flush
			var results;
			core.startMoving(params, function onResult(err, result) {
				core.debug("<startMovingSync|onResult>", err, result);
				results = Array.prototype.slice.call(arguments);
			});
			do {
				skapi.WaitEvent(100, wemSingle);
			} while (typeof results === "undefined");
			var err = results && results.length > 0 && results[0];
			if (err) throw err;
			return results && results.length > 1 && results[1];
		};
	}();

	core.clone = function clone(obj) {
		// Clone a object.
		var copy;

		// Handle the 3 simple types, and null or undefined
		if (null == obj || "object" != typeof obj) return obj;

		// Handle Date
		if (obj instanceof Date) {
			copy = new Date();
			copy.setTime(obj.getTime());
			return copy;
		}

		// Handle Array
		if (obj instanceof Array) {
			copy = [];
			// eslint-disable-next-line no-restricted-syntax
			for (var i = 0, len = obj.length; i < len; i++) {
				copy[i] = core.clone(obj[i]);
			}
			return copy;
		}

		// Handle Object
		if (obj instanceof Object) {
			copy = {};
			for (var attr in obj) {
				if (obj.hasOwnProperty(attr)) copy[attr] = core.clone(obj[attr]);
			}
			return copy;
		}

		throw new Error("Unable to copy obj! Its type isn't supported.");
	};

	core.difference = function difference(subjects) {
		var length = subjects.length;
		var ref = subjects[0];
		var diff = {};
		var c;
		var keys;
		var keysLength;
		var key;
		var u;

		// eslint-disable-next-line no-restricted-syntax
		for (var i = 1; i < length; i++) {
			c = subjects[i];
			keys = Object.keys(c);
			keysLength = keys.length;

			// eslint-disable-next-line no-restricted-syntax
			for (u = 0; u < keysLength; u++) {
				key = keys[u];

				if (!core.isStrictEqual(c[key], ref[key]))
					diff[key] = c[key];
			}
		}

		return diff;
	};

	core.isStrictEqual = function isStrictEqual(a, b) {
		return a === b;
	};

	/**
	 * Cancellation() Object passed between multiple async functions to indicate that we want to cancel a current process.
	 *
	 * @return {object} Object containing 'canceled' boolean.
	 */
	core.Cancellation = (function factory() {
		function Cancellation() {
			this.canceled = false;
		}
		return Cancellation;
	})();
	
	
	// Initalize
	skapi.AddHandler(evidOnTipMessage,          core);
	skapi.AddHandler(evidOnTimer,               core);
	skapi.AddHandler(evidOnStartPortalSelf,		core);
	
	// Setup footprint timer to track our steps. 
	var footstepTimer = skapi.TimerNew();
	footstepTimer.tag = "SkunkNavigation_" + core.simpleUUID();
	footstepTimer.cmsec = -1;
	
	AnimationFilter.addCallback(onAnimationGlobal);
		
	return core;
}));