/********************************************************************************************\
	File Name:      AnimationFilter.js
	Purpose:        Watch raw server messages for animation effects.
	Creator:        Cyprias
	Date:           04/01/2015
	License:        MIT License	(http://opensource.org/licenses/MIT)
\*********************************************************************************************/

var MAJOR = "AnimationFilter-1.1";
var MINOR = 200209;

(function (factory) {
	// Check if script was loaded via require().
	if (typeof module === 'object' && module.exports) {
		module.exports = factory();
	} else {
		// Script was loaded via swx file, set as a global object.
		// eslint-disable-next-line no-undef
		AnimationFilter11 = factory();
	}
}(function () {
	var core;
	if (typeof LibStub !== "undefined") {
		core = LibStub.newLibrary(MAJOR, MINOR);
		if (!core) return LibStub(MAJOR);
	} else {
		core = {};
	}
	core.MAJOR = MAJOR;
	core.MINOR = MINOR;
	
	var debugging = false;

	var mtyAnimation        = 0xF74C0000 - 0x100000000;    // Event ID for animation raw server messages.

	core.debug = function debug(szMsg) {
		if (debugging == true) {
			skapi.OutputSz(core.MAJOR + ": " + szMsg + "\n", opmConsole);
		}
	};

	var animationTypes = core.animationTypes = {};// These names are guesses of what I've seen them fire for. They're not official names. 
	//animationTypes.GENERIC             = 0x0000;
	//animationTypes.MOVE_TO_OBJECT      = 0x0006;
	//animationTypes.FACE_COORDINATE     = 0x0007; // ?
	//animationTypes.FACE_OBJECT         = 0x0008;

	animationTypes.Generic                       = 0x0;
	animationTypes.RawCommand                    = 0x1;
	animationTypes.InterpretedCommand            = 0x2;
	animationTypes.StopRawCommand                = 0x3;
	animationTypes.StopInterpretedCommand        = 0x4;
	animationTypes.StopCompletely                = 0x5;
	animationTypes.MoveToObject                  = 0x6;
	animationTypes.MoveToPosition                = 0x7;
	animationTypes.TurnToObject                  = 0x8;
	animationTypes.TurnToHeading                 = 0x9;

	var handler = {};
	handler.OnRawServerMessage = function fOnRawServerMessage(mty, mbuf) {
		switch(mty) {
		case mtyAnimation:
			var payload = {};
			var complete = false;
			mbuf.SkipCb(4); // Skip over the header

			
			payload.object              = mbuf.Get_DWORD();
			payload.logins              = mbuf.Get_WORD();
			payload.sequence            = mbuf.Get_WORD();
			payload.index               = mbuf.Get_WORD();
			payload.activity            = mbuf.Get_WORD();
			payload.animation_type      = mbuf.Get_BYTE();
			payload.type_flags          = mbuf.Get_BYTE();
			payload.stance              = mbuf.Get_WORD();

			if (payload.animation_type == animationTypes.Generic) {
				payload.flags = mbuf.Get_DWORD();
				
				// These need to be individual if statments.
				if (payload.flags & 0x00000001) {
					payload.stance2 = mbuf.Get_WORD();
				}
				if (payload.flags & 0x00000002) {
					payload.animation_1 = mbuf.Get_WORD();
				}
				if (payload.flags & 0x00000008) {
					payload.animation_2 = mbuf.Get_WORD();
				}
				if (payload.flags & 0x00000020) {
					payload.animation_3 = mbuf.Get_WORD();
				}
				if (payload.flags & 0x00000004) {
					payload.float_1 = mbuf.Get_float();
				}
				if (payload.flags & 0x00000010) {
					payload.float_2 = mbuf.Get_float();
				}
				if (payload.flags & 0x00000040) {
					payload.float_3 = mbuf.Get_float();
				}
				if (payload.flags & 0x00000F80) {
					payload.animation = mbuf.Get_WORD();
					payload.sequence = mbuf.Get_WORD();
					payload.animation_speed = mbuf.Get_float();
				}
			}
			if (payload.animation_type == animationTypes.MoveToObject) {	//0x0006
				payload.target = mbuf.Get_DWORD();
				payload.landblock = mbuf.Get_DWORD();
				payload.xOffset = mbuf.Get_float();
				payload.yOffset = mbuf.Get_float();
				payload.zOffset = mbuf.Get_float();
				payload.flags_2 = mbuf.Get_DWORD();
				payload.float_1 = mbuf.Get_float();
				payload.float_2 = mbuf.Get_float();
				payload.unknown_3 = mbuf.Get_DWORD();
				payload.animation_speed = mbuf.Get_float();
				payload.float_4 = mbuf.Get_float();
				payload.heading = mbuf.Get_float();
				payload.unknown_value = mbuf.Get_DWORD();
			}
			if (payload.animation_type == animationTypes.MoveToPosition) {	//0x0007
				payload.landblock = mbuf.Get_DWORD();
				payload.xOffset = mbuf.Get_float();
				payload.yOffset = mbuf.Get_float();
				payload.zOffset = mbuf.Get_float();
				payload.flags_2 = mbuf.Get_DWORD();
				payload.float_1 = mbuf.Get_float();
				payload.float_2 = mbuf.Get_float();
				payload.unknown_3 = mbuf.Get_DWORD();
				payload.animation_speed = mbuf.Get_float();
				payload.float_4 = mbuf.Get_float();
				payload.heading = mbuf.Get_float();
				payload.unknown_4 = mbuf.Get_DWORD();
			}
			if (payload.animation_type == animationTypes.TurnToObject) { //0x0008
				payload.target = mbuf.Get_DWORD();
				payload.unknown_value = mbuf.Get_DWORD();
				payload.flags_2 = mbuf.Get_DWORD();
				payload.animation_speed = mbuf.Get_float();
				payload.heading = mbuf.Get_float();
			}
			if (payload.animation_type == 0x0009) { //0x0009
				payload.flags_2 = mbuf.Get_DWORD();
				payload.animation_speed = mbuf.Get_float();
				payload.heading = mbuf.Get_float();
			}

			mbuf.Align_DWORD();
			if (payload.type_flags & 0x01) {
				payload.targetid = mbuf.Get_DWORD();
			}

			var complete = (mbuf.ibInMsg / mbuf.cbOfMsg) * 100;
			if (complete < 100) {
				core.debug("<AnimationFilter> Something's uncaught! ibInMsg: " + mbuf.ibInMsg + ", cbOfMsg: " + mbuf.cbOfMsg + ", complete: " + ((mbuf.ibInMsg / mbuf.cbOfMsg) * 100));
				if (JSON && debugging) {
					core.debug(JSON.stringify(payload, null, "\t"));
				}
			}
			complete = true;
		
			if (complete == true) {
				// No try catch for firing callback.
				fireCallback(payload);
			}
		}
	};

	var _callbacks = [];
	core.addCallback = function addCallback(method, thisArg) {
		_callbacks.push({method: method, thisArg: thisArg});
		if (_callbacks.length == 1) {
			skapi.AddHandler(mtyAnimation,  handler);
		}
	};

	core.removeCallback = function removeCallback(method, thisArg) {
		// eslint-disable-next-line no-restricted-syntax
		for (var i = _callbacks.length - 1; i >= 0; i--) {
			if (_callbacks[i].method == method) {
				if (thisArg && thisArg != _callbacks[i].thisArg) continue;
				_callbacks.splice(i, 1);
			}
		}
		if (_callbacks.length == 0) {
			skapi.RemoveHandler(mtyAnimation,  handler);
		}
	};

	function fireCallback() {
		// eslint-disable-next-line no-restricted-syntax
		for (var i = _callbacks.length - 1; i >= 0; i--) {
			_callbacks[i].method.apply(_callbacks[i].thisArg, arguments);
		}
	}

	return core;
}));