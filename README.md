# SkunkNavigation.js, Copyright 2015	cyprias@gmail.com

## Synopsis

Navigation library for Skunkworks.

### 2020/03/09: The documentation below may be out of date.

## Functionality
- Navigate to a specific maploc.
- Auto detect and open doors ahead of us.
- Monitor animations to determin our current stance (running/walking/turning/standing).
- Stuck detection and wobble to get unstuck.
- Backwards compatible with SkunkNav route files.
- When turning while moving, estimate how far we'll travel away from a line to determin whether we should stop moving and complete the turn.
- Auto decide when to walk or run when navigating.
- When navigating a SkunkNav route, we determin whether we need to stop at a waypoint or if we can continue moving based on our speed and how far turning will move us away from a line between waypoints.

## API
#### API for global SkunkNavigation object.
`<Required> [optional]`

- ` SkunkNavigation.usePortalAco(<acoPortal>)`: Use a portal and wait to exit.
- ` SkunkNavigation.stopWalking()`: Stop walking and wait for animation to confirm.
- ` SkunkNavigation.stopMoving()`: Stop moving and wait for animation to confirm.
- ` SkunkNavigation.stopAutoRunning()`: Stop autorunning and wait for animation to confirm.
- ` SkunkNavigation.startWalking()`: Start walking and wait for animation to confirm.
- ` SkunkNavigation.startMoving([toMaploc])`: Start autorunning, if maploc specified we'll walk if distance is short.
- ` SkunkNavigation.startAutoRunning([toMaploc])`: Start autorunning and wait for animation to confirm.
- ` SkunkNavigation.setMonitorMethod(<func>)`: Assign a function to be monitored while the engine runs, if value changes we exit the function call.
- `SkunkNavigation.routeFromFile(<filePath>)`: Return our own Route object of a SkunkNav route file.
- `SkunkNavigation.openDoor(acoDoor)`: Assess the door and open it if it's closed.
- `SkunkNavigation.headingTurn(<heading>)`: Return the angle from our current heading needed to face the intended heading. Positive number = turn right, negitive number = turn left.
- `SkunkNavigation.goToMaploc(<maploc>, [distArrive], [fWalk], [fStopOnArrival], [cmsecTimeout], [startingMaploc])`: Travel to a maploc. 
- `SkunkNavigation.getTurnPositions(<turnRight>, <degrees>, [distance])`: Return array of coordinates that we'll move to when turning while moving.
- `SkunkNavigation.getDoorsNearMaploc([maploc])`: Return coaco of doors within 1 meter of a maploc.
- `SkunkNavigation.getDoorsNearLine(<maplocA>, <maplocB>)`: Return coaco of doors within 1 meter of a line between two maplocs.
- `SkunkNavigation.flags`: Result bitwise flags returned by various functions.
- `SkunkNavigation.face(<heading>, [variance])`: Face a heading in degrees.
- `SkunkNavigation.Course`: Main Course object used by engine. Allows you to handle ticks() within your own function loop. 
- `SkunkNavigation.amWalking()`: Return true/false if we're walking (based on animations).
- `SkunkNavigation.amRunning()`: Return true/false if we're running (based on animations).
- `SkunkNavigation.amMoving()`: Return true/false if we're moving (based on animations).
- `SkunkNavigation.amAutoRunning()`: Return true/false if we're auto running (based on tooltip).
- `SkunkNavigation.getETA()`: Returns how long it'll take to run to a maploc.

#### API for Route objects.
- `route.Go()`: Start traveling waypoints of the loaded SkunkNav route. Returns status flags (result & SkunkNavigation.flags.x)
- `route.RouteReverse()`: Reverse the current route.
- `route.GetRawRoute()`: Return raw route object from SkunkNav's RouteFromFile() function.
- `route.GetFirstMaploc()`: Return the first maploc waypoint in the route.
- `route.GetLastMaploc()`: Return the first last waypoint in the route.
- `route.GetMaploc(<index>)`: Return the maploc of a specific index.
- `route.GetMaplocCount()`: Return number of maplocs in the route.

#### API for Course object.
- `course.tick()`: Execute the engine's navigation logic one time.
- `course.start()`: Tell the course you're starting, needed for assigning certain internal variables.
- `course.setZigZag(<value>)`: Enable randomly strafing back and forth while moving, for dodging projectiles.
- `course.setTimeout(<miliseconds>)`: Set timeout.
- `course.setStartingMaploc(<maploc>)`: Assign a starting maploc, used for plotting a line to the target maploc to know if we're too far away from path.
- `course.setMaploc(<maploc>)`: Set the target maploc.
- `course.setDistArrive(<mapUnits>)`: Set how close we need to be to consider ourselves arrived.
- `course.setDirection(<degrees>)`: Set direction we want to be heading in, if no target maploc is supplied.
- `course.resume()`: Resume the current navigation, used for tracking pause time to be ignored by timeout calculation.
- `course.pause()`: Tell course we're pausing, to ignore this period of time in timeout calc.
- `course.hasTimedout()`: Return true/false if the course has timed out.
- `course.hasArrived()`: Return true/false if the course has arrived at the target maploc.
- `course.getTickCount()`: Return number of times the course's tick() function has been called.
- `course.getStuckElapsed()`: Return how long we've been stuck for.
- `course.getMaploc()`: Return the target maploc.
- `course.getAge()`: Return how old this course is (based on the object's creation time).
- `course.distanceToMaploc()`: Return the distance in mapunits to the target maploc.
- `course.setAco(<aco>)`: Set an aco whos maploc we we want to navigate to, good for moving targets.
- 
## License
MIT License	(http://opensource.org/licenses/MIT)